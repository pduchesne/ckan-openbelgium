=================================================
ckanext-openbelgium - OpenBelgium profile of CKAN
=================================================

This extension defines one plugin to activate to apply the OpenBelgium theme and features:

* OpenBelgiumExtension (``openbelgium_extension``)

About the features
====================

UI Theme
--------


Imported plugins
----------------

ckanext-harvest

ckanext-spatial